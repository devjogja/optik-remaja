<!DOCTYPE html>
<html lang="{!! str_replace('_', '-', app()->getLocale()) !!}">
    <head>
        <meta charset="utf-8" />
        <title>
            {!! Config::get('app.name') !!} | {!! $title ?? '' !!}
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        @include('layouts.styles')
    </head>
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
                <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                    <div class="m-stack m-stack--hor m-stack--desktop">
                        <div class="m-stack__item m-stack__item--fluid">
                            <div class="m-login__wrapper">
                                <div class="m-login__logo">
                                    <a href="{!! url('/'); !!}">
                                        {!! Html::image('assets/app/media/img/logos/logo-2.png','Logo', array()) !!}
                                    </a>
                                </div>
                                <div class="m-login__signin">
                                    <div class="m-login__head">
                                        <h3 class="m-login__title">
                                            Login
                                        </h3>
                                    </div>
                                    {!! Form::open(['method' => 'POST', 'url' => 'login', 'class'=>'m-login__form m-form']) !!}
                                        <div class="form-group m-form__group">
                                            {!! Form::text('email', null, ['class'=>'form-control m-input', 'placeholder'=>'E-Mail / Username', 'autocomplete'=>'off']) !!}
                                        </div>
                                        <div class="form-group m-form__group">
                                            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                                        </div>
                                        <div class="row m-login__form-sub">
                                            <div class="col m--align-left">
                                                <label class="m-checkbox m-checkbox--focus">
                                                    <input type="checkbox" name="remember">
                                                    Remember me
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="m-login__form-action">
                                            {!! Form::submit('Login', ['id'=>'m_login_signin_submit', 'class'=>'btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air']) !!}
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1  m-login__content" style="background-image: url(../../../assets/app/media/img/bg/bg-4.jpg)">
                    <div class="m-grid__item m-grid__item--middle">
                        <h3 class="m-login__welcome">
                            {!! Config::get('app.name') !!}
                        </h3>
                        <p class="m-login__msg">
                            {!! Config::get('app.desc') !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.script')
        {!! Html::script('assets/snippets/pages/user/login.js') !!}
    </body>
</html>