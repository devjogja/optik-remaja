<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
	<li class="m-menu__item"  aria-haspopup="true" amenu="home">
		<a  href="{!! url('/'); !!}" class="m-menu__link ">
			<i class="m-menu__link-icon flaticon-line-graph"></i>
			<span class="m-menu__link-text">
				Dashboard
			</span>
		</a>
	</li>
</ul>

@push('scripts')
<script type="text/javascript">
	$(function(){
		var uri_segment = '{!! Request::segment(1) !!}';
		if (uri_segment == '') {uri_segment = 'home';}
		$('.m-menu__item').removeClass('m-menu__item--active');
		$('.m-menu__nav li[amenu='+uri_segment+']').addClass('m-menu__item--active');
	});
</script>
@endpush