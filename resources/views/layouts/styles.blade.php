{!! Html::script('js/webfont.js') !!}
<script>
WebFont.load({
    google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
    active: function() {
        sessionStorage.fonts = true;
    }
});
</script>
<!--end::Web font -->
<!--begin::Base Styles -->
{!! Html::style('assets/vendors/base/vendors.bundle.css') !!}
{!! Html::style('assets/default/base/style.bundle.css') !!}
<!--end::Base Styles -->
{!! Html::favicon('assets/default/media/img/logo/favicon.ico') !!}
