@extends('layouts.base')
@section('content')
<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Local Datatable
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<div class="m_datatable" id="local_data"></div>
	</div>
</div>
@endsection
@push('scripts')
{!! Html::script('assets/default/custom/components/datatables/base/data-local.js') !!}
@endpush